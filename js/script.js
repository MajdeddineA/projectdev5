const APIKEY = '5aae478eeffc568de0a3970ce3cdb956'; // Key tmdb

// Enter text box after writting the name of the movie but not working with the page number generater
/* $(document).ready(() => {
  $('#searchMovie').on('submit', (e) => {
    const searchText = $('#searchText').val();
    getMovies(searchText);
    e.preventDefault();
  });
});
//function search for all movies by the name
function getMovies(searchText) {
  fetch(`https://api.themoviedb.org/3/search/movie?api_key=${APIKEY}&language=fr-FR&page=1&include_adult=true&query=${searchText}`)

    .then((data) => {
      if (data.ok === true) {
        return data.json().then((res) => {
          console.log(res);
          const result = document.querySelector('#result');

          const promise = new Promise((r, e) => {
            refreshSearch(searchText, result);
            return r();
          });

          promise.then(() => {
            if (res.total_results == 0) return noMovieResults(result);
            return MovieResults(result, res);
          });
        });
      }
      return 'alert';
    });
}

const refreshSearch = (searchText, pnl) => {
  const result = document.querySelector('#result');
};

const noMovieResults = (pnl) => {
  const p = document.createElement('p');
  p.innerHTML = 'No film with the same name.';
  pnl.append(p);
};

//movie result
let output = '';
const MovieResults = (pnl, res) => {

  for (let i = 0; i < res.results.length; i++) {

    output += `
            <div class="row" style="margin: 2em;">
            <div class="col-3">
                <img src=${`https://image.tmdb.org/t/p/w220_and_h330_face/${res.results[i].poster_path}`}>
            </div>

            <div class="col">
                <h5>${res.results[i].original_title}</h5>
                <h5>${`Rate: ${res.results[i].vote_average}`}</h5>
                <h5>${`${res.results[i].overview}`}</h5>
                <a onclick="movieSelected('${res.results[i].id}')" class="btn btn-primary" href="#">Movie Details</a>
            </div>
            </div>
          `;
  }
  $('#movies').html(output);

}; */

const searchMovie = document.getElementById('searchMovie');
const searchActor = document.getElementById('searchActor');
const searchTV = document.getElementById('searchTV');
const popularmovies = document.getElementById('popularmovies');
searchMovie.style.display = 'none';
searchActor.style.display = 'none';
searchTV.style.display = 'none';
popularmovies.style.display = 'none';

// choose form dropdown box
$(document).ready(() => {
  $('#continents').change(function () {
    const el = $(this);

    if (el.val() === 'Movies') {
      $('#message').html('');
      $('#tvs').html('');
      $('#popularmovies').html('');

      $('#pagination1').twbsPagination('destroy');
      $('#pagination2').twbsPagination('destroy');
      $('#pagination3').twbsPagination('destroy');

      searchMovie.style.display = 'block';

      searchActor.style.display = 'none';
      searchTV.style.display = 'none';
      popularmovies.style.display = 'none';
    } else if (el.val() === 'Actors') {
      $('#movies').html('');
      $('#tvs').html('');
      $('#popularmovies').html('');

      $('#pagination').twbsPagination('destroy');
      $('#pagination2').twbsPagination('destroy');
      $('#pagination3').twbsPagination('destroy');

      searchMovie.style.display = 'none';
      searchTV.style.display = 'none';
      popularmovies.style.display = 'none';

      searchActor.style.display = 'block';
    } else if (el.val() === 'Tv series') {
      $('#movies').html('');
      $('#message').html('');
      $('#popularmovies').html('');

      $('#pagination').twbsPagination('destroy');
      $('#pagination1').twbsPagination('destroy');
      $('#pagination3').twbsPagination('destroy');

      searchMovie.style.display = 'none';
      searchActor.style.display = 'none';
      popularmovies.style.display = 'none';

      searchTV.style.display = 'block';
    } else if (el.val() === 'Popular movies') {
      $('#movies').html('');
      $('#message').html('');
      $('#tvs').html('');

      $('#pagination').twbsPagination('destroy');
      $('#pagination1').twbsPagination('destroy');
      $('#pagination2').twbsPagination('destroy');

      searchMovie.style.display = 'none';
      searchActor.style.display = 'none';
      searchTV.style.display = 'none';

      popularmovies.style.display = 'block';

      // popular movies
      getPopular(1);
      function getPopular(page) {
        fetch(`https://api.themoviedb.org/3/movie/popular?api_key=${APIKEY}&language=en-US&page=${page}`)

          .then((data) => {
            if (data.ok === true) {
              return data.json().then((res) => {
                console.log(res);

                let output = '';
                for (let i = 0; i < res.results.length; i++) {
                  output += `
                            <div class="row" style="margin: 2em;">
                            <div class="col-3">
                                <img src=${`https://image.tmdb.org/t/p/w220_and_h330_face/${res.results[i].poster_path}`}>
                            </div>
                
                            <div class="col">
                                <h5>${res.results[i].original_title}</h5>
                                <h5>${`Rate: ${res.results[i].vote_average}`}</h5>
                                <h5>${`${res.results[i].overview}`}</h5>
                                <a onclick="movieSelected('${res.results[i].id}')" class="btn btn-primary" href="#">Movie Details</a> 
                            </div>
                            </div>
                          `;
                }
                $('#popularmovies').html(output);

                Paging(res.total_pages);
              });
            }
          });
      }

      function Paging(totalPage) {
        const obj = $('#pagination3').twbsPagination({
          totalPages: totalPage,
          visiblePages: 5,
          onPageClick(event, page) {
            getPopular(page);
          },
        });
      }
    }
  });
});

// Enter text box after writting the name of the movie, good good
$(document).ready(() => {
  $('#searchMovie').on('submit', (e) => {
    // var searchText = $('#searchText').val();
    getMovies(1);
    e.preventDefault();
  });

  // function search for all movies by the name
  function getMovies(page) {
    fetch(`https://api.themoviedb.org/3/search/movie?api_key=${APIKEY}&language=fr-FR&page=${page}&include_adult=true&query=${$('#searchText').val()}`)

      .then((data) => {
        if (data.ok === true) {
          return data.json().then((res) => {
            console.log(res);
            const result = document.querySelector('#result');

            let output = '';
            for (let i = 0; i < res.results.length; i++) {
              output += `
                      <div class="row" style="margin: 2em;">
                      <div class="col-3">
                          <img src=${`https://image.tmdb.org/t/p/w220_and_h330_face/${res.results[i].poster_path}`}>
                      </div>
          
                      <div class="col">
                          <h5>${res.results[i].original_title}</h5>
                          <h5>${`Rate: ${res.results[i].vote_average}`}</h5>
                          <h5>${`${res.results[i].overview}`}</h5>
                          <a onclick="movieSelected('${res.results[i].id}')" class="btn btn-primary" href="#">Movie Details</a> 
                      </div>
                      </div>
                    `;
            }
            $('#movies').html(output);

            Paging(res.total_pages);
          });
        }
        return 'alert';
      });
  }

  const refreshSearch = (searchText, pnl) => {
    const result = document.querySelector('#result');
  };

  const noMovieResults = (pnl) => {
    const p = document.createElement('p');
    p.innerHTML = 'No film with the same name.';
    pnl.append(p);
  };

  function Paging(totalPage) {
    const obj = $('#pagination').twbsPagination({
      totalPages: totalPage,
      visiblePages: 5,
      onPageClick(event, page) {
        getMovies(page);
      },
    });
  }
});

// For selected movie
function movieSelected(id) {
  sessionStorage.setItem('movieId', id);
  console.log(id);
  window.location = 'movie.html';
  return false;
}

// get the selected movie
function getMovie() {
  const movieId = sessionStorage.getItem('movieId');

  fetch(`https://api.themoviedb.org/3/movie/${movieId}?api_key=${APIKEY}`)
    .then((response) => {
      console.log(response);
      if (response.ok === true) {
        return response.json().then((resu) => {
          console.log(resu);

          let element = '';
          for (let i = 0; i < resu.genres.length; i++) {
            element += `${resu.genres[i].name} `;
          }
          // console.log(element)

          let elementt = '';
          for (let j = 0; j < resu.spoken_languages.length; j++) {
            elementt += `${resu.spoken_languages[j].name} `;
          }
          // console.log(elementt)

          const output = `
        <div class="row">
          <div class="col-md-4">
            <img src="https://image.tmdb.org/t/p/w220_and_h330_face/${resu.poster_path}" class="thumbnail">
          </div>
          <div class="col-md-8">
            <h2>${resu.original_title}</h2>
            <ul class="list-group">
              <li class="list-group-item"><strong>Genre:</strong> ${element}</li>
              <li class="list-group-item"><strong>Released:</strong> ${resu.release_date}</li>
              <li class="list-group-item"><strong>Rated:</strong> ${resu.popularity}</li>
              <li class="list-group-item"><strong>Tagline:</strong> ${resu.tagline}</li>
              <li class="list-group-item"><strong>Spoken languages:</strong> ${elementt}</li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="well">
            <h3>Plot</h3>
            ${resu.overview}
            <hr>
            <a href="http://imdb.com/title/${resu.imdb_id}" target="_blank" class="btn btn-primary">View IMDB</a>
            <a href="index.html" class="btn btn-default">Go Back To Search</a>
          </div>
        </div>
      `;

          $('#movie').html(output);
        });
      }
    })
    .catch((err) => {
      console.log(err);
    });
}

// actor search with button and ajax
/* $(document).ready(function () {
  $("#submit").click(function (e) {
      var validate = Validate();
      $("#message").html(validate);
      if (validate.length == 0) {
          CallAPI(1);
      }
  });

  function CallAPI(page) {
      $.ajax({
          url: "https://api.themoviedb.org/3/search/person?language=en-US&query=" + $("#searchActor").val() + "&page=" + page + "&include_adult=false",
          data: { "api_key": "5aae478eeffc568de0a3970ce3cdb956" },
          dataType: "json",
          success: function (result, status, xhr) {
            console.log(result)
              var resultHtml = $("<div class=\"resultDiv\"><p>Names</p>");
              for (i = 0; i < result["results"].length; i++) {

                  var image = result["results"][i]["profile_path"] == null ? "Image/no-image.png" : "https://image.tmdb.org/t/p/w500/" + result["results"][i]["profile_path"];

                  resultHtml.append("<div class=\"result\" resourceId=\"" + result["results"][i]["id"] + "\">" + "<img src=\"" + image + "\" />" + "<p><a>" + result["results"][i]["name"] + "</a></p></div>")
              }

              resultHtml.append("</div>");
              $("#message").html(resultHtml);

              Paging(result["total_pages"]);
          },
          error: function (xhr, status, error) {
              $("#message").html("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText)
          }
      });
  }

  function Validate() {
      var errorMessage = "";
      if ($("#searchActor").val() == "") {
          errorMessage += "► Enter Search Text";
      }
      return errorMessage;
  }

  function Paging(totalPage) {
      var obj = $("#pagination1").twbsPagination({
          totalPages: totalPage,
          visiblePages: 5,
          onPageClick: function (event, page) {
              CallAPI(page);
          }
      });
  }
});

//actor result
$("#message").on("click", ".result", function () {
  var resourceId = $(this).attr("resourceId");
  $.ajax({
      url: "https://api.themoviedb.org/3/person/" + resourceId + "?language=en-US",
      data: {
          api_key: "5aae478eeffc568de0a3970ce3cdb956"
      },
      dataType: 'json',
      success: function (result, status, xhr) {
          $("#modalTitleH4").html(result["name"]);

          var image = result["profile_path"] == null ? "Image/no-image.png" : "https://image.tmdb.org/t/p/w500/" + result["profile_path"];
          var biography = result["biography"] == null ? "No information available" : result["biography"];

          var resultHtml = "<p class=\"text-center\"><img src=\"" + image + "\"/></p><p>" + biography + "</p>";

          $("#modalBodyDiv").html(resultHtml)

          $("#myModal").modal("show");
      },
      error: function (xhr, status, error) {
          $("#message").html("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText)
      }
  });
}); */

// actor search
$(document).ready(() => {
  $('#searchActor').on('submit', (e) => {
    CallAPI(1);
    e.preventDefault();
  });

  function CallAPI(page) {
    fetch(`https://api.themoviedb.org/3/search/person?api_key=${APIKEY}&page=${page}&include_adult=false&query=${$('#searchText1').val()}`)

      .then((data) => {
        if (data.ok === true) {
          return data.json().then((resul) => {
            console.log(resul);

            // const result = document.querySelector('#message');

            let output = '';
            for (let i = 0; i < resul.results.length; i++) {
              output += `<div class="col-md-6">
                            <div class="well text-center">
                              <a onclick="personSelected('${resul.results[i].id}')" class="btn1" href="#"><img src=${`https://image.tmdb.org/t/p/w500/${resul.results[i].profile_path}`} style="width:60%"></a>
                              
                              <h5>${resul.results[i].name}</h5>
                              <h5>${`${resul.results[i].known_for_department}`}</h5>
                              
                         
                            </div>
                        </div>`;
            }
            $('#message').html(output);

            Paging(resul.total_pages);
          });
        }
      });
  }

  function Paging(totalPage) {
    const obj = $('#pagination1').twbsPagination({
      totalPages: totalPage,
      visiblePages: 5,
      onPageClick(event, page) {
        CallAPI(page);
      },
    });
  }
});

// For selected person
function personSelected(id) {
  fetch(`https://api.themoviedb.org/3/person/${id}?api_key=${APIKEY}&language=en-US`)
    .then((response) => {
      console.log(response);
      if (response.ok === true) {
        return response.json().then((resultt) => {
          console.log(resultt);

          $('#modalTitleH4').html(resultt.name);

          const image = resultt.profile_path == null ? 'Image/no-image.png' : `https://image.tmdb.org/t/p/w500/${resultt.profile_path}`;
          const biography = resultt.biography == null ? 'No information available' : resultt.biography;

          const resultHtml = `<p class="text-center"><img src="${image}" style='width:80%'/></p><p>${biography}</p>`;

          $('#modalBodyDiv').html(resultHtml);

          $('#myModal').modal('show');
        });
      }
    });
}

// TV search
$(document).ready(() => {
  $('#searchTV').on('submit', (e) => {
    getTvs(1);
    e.preventDefault();
  });

  // function search for all movies by the name
  function getTvs(page) {
    fetch(`https://api.themoviedb.org/3/search/tv?api_key=${APIKEY}&language=en-US&page=${page}&include_adult=false&query=${$('#searchText2').val()}`)

      .then((data) => {
        if (data.ok === true) {
          return data.json().then((res) => {
            console.log(res);
            const result = document.querySelector('#result');

            let output = '';
            for (let i = 0; i < res.results.length; i++) {
            // let image1 = res.results[i].poster_path == null ? "Image/no-image.png" : "https://image.tmdb.org/t/p/w220_and_h330_face" + res.results[i].profile_path;
              // <img src=${image1}></img>

              output += `
                      <div class="row" style="margin: 2em;">
                      <div class="col-3">
                          <img src=${`https://image.tmdb.org/t/p/w220_and_h330_face/${res.results[i].poster_path}`}>
                      </div>
          
                      <div class="col">
                          <h5>${res.results[i].original_name}</h5>
                          <h5>${`Rate: ${res.results[i].vote_average}`}</h5>
                          <h5>${`${res.results[i].overview}`}</h5>
                          <a onclick="tvSelected('${res.results[i].id}')" class="btn btn-primary" href="#">Tv Details</a> 
                      </div>
                      </div>
                    `;
            }
            $('#tvs').html(output);

            Paging(res.total_pages);
          });
        }
        return 'alert';
      });
  }

  const refreshSearch = (searchText, pnl) => {
    const result = document.querySelector('#result');
  };

  const noTvResults = (pnl) => {
    const p = document.createElement('p');
    p.innerHTML = 'No film with the same name.';
    pnl.append(p);
  };

  function Paging(totalPage) {
    const obj = $('#pagination2').twbsPagination({
      totalPages: totalPage,
      visiblePages: 5,
      onPageClick(event, page) {
        getTvs(page);
      },
    });
  }
});

// For selected tv
function tvSelected(id) {
  sessionStorage.setItem('tvId', id);
  console.log(id);
  window.location = 'tv.html';
  return false;
}

// get the selected tv
function getTvs() {
  const tvId = sessionStorage.getItem('tvId');

  fetch(`https://api.themoviedb.org/3/tv/${tvId}?api_key=${APIKEY}&language=en-US`)
    .then((response) => {
      console.log(response);
      if (response.ok === true) {
        return response.json().then((resu) => {
          console.log(resu);

          let element2 = '';
          for (let i = 0; i < resu.production_companies.length; i++) {
            element2 += `${resu.production_companies[i].name} `;
          }
          // console.log(element)

          const output = `
        <div class="row">
          <div class="col-md-4">
            <img src="https://image.tmdb.org/t/p/w220_and_h330_face/${resu.backdrop_path}" class="thumbnail">
          </div>
          <div class="col-md-8">
            <h2>${resu.original_name}</h2>
            <ul class="list-group">
              <li class="list-group-item"><strong>Production companies:</strong> ${element2}</li>
              <li class="list-group-item"><strong>Next episode to air:</strong> ${resu.next_episode_to_air}</li>
              <li class="list-group-item"><strong>Rated:</strong> ${resu.popularity}</li>
              <li class="list-group-item"><strong>Number of seasons:</strong> ${resu.number_of_seasons}</li>
              <li class="list-group-item"><strong>Status:</strong> ${resu.status}</li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="well">
            <h3>Plot</h3>
            ${resu.overview}
            <hr>
            
            <a href="index.html" class="btn btn-default">Go Back To Search</a>
          </div>
        </div>
      `;

          $('#tv').html(output);
        });
      }
    })
    .catch((err) => {
      console.log(err);
    });
}
